from django.apps import AppConfig


class Miniapp2Config(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "miniapp2"
