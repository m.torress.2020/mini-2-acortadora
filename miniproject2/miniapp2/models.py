from django.db import models


# Create your models here.
class ShortUrl(models.Model):
    objects = None
    url = models.CharField(max_length=128, default="")
    content = models.CharField(max_length=128, blank=True)
    shortened = models.CharField(max_length=64, unique=True)

    def __str__(self):
        return self.url
