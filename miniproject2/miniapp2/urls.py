from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("<content>", views.redirect_url, name="redirect"),
    path('favicon.ico', views.get_favicon),
]

